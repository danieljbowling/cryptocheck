module.exports = function(app, _params) {
	
	var Util = require('./modules/Util.js');
	Util.init({mysql: _params.mysql});
	app.use('/api/util', Util);

	var User = require('./modules/User.js');
	User.init({util: Util});
	app.use('/api/user', User);

	var Wallet = require('./modules/Wallet.js');
	Wallet.init({util: Util});
	app.use('/api/wallet', Wallet);

	app.get('*', function(req, res) {	// Only allowed to access with secure
		if( _params.run_https && !req.secure ){
			var url = 'https://' + req.headers.host.replace(':'+app.http_port, '');
				url+= ':' + app.https_port +  req.url;
			res.redirect( url );
		}else{
			var path = require('path');
			res.sendFile(path.resolve('public/index.html'));
		}
	});

};