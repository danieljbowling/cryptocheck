var express = require('express');  
var router = express.Router();

var request = require('request');

router.get('/', function(req, res, next) {
	res.send('Wallet module');
});

router.post('/add', function(req, res, next) {
	if( !req.session.me || req.session.me == null ){
		res.send( JSON.stringify({error:true, reason:"login"}) );
	}else{
		req.body.user_id = req.session.me;
		Wallet.add( req.body, function(_data){
			res.send( JSON.stringify(_data) );
		})
	}
});
router.post('/update', function(req, res, next) {
	if( !req.session.me || req.session.me == null ){
		res.send( JSON.stringify({error:true, reason:"login"}) );
	}else{
		req.body.user_id = req.session.me;
		Wallet.update( req.body, function(_data){
			res.send( JSON.stringify(_data) );
		})
	}
});

router.get('/list', function(req, res, next) {
	if( !req.session.me || req.session.me == null ){
		res.send( JSON.stringify({error:true, reason:"login"}) );
	}else{
		Wallet.list( {user_id: req.session.me}, function(_data){
			res.send( JSON.stringify(_data) );
		});
	}
});

router.get('/value', function(req, res, next) {
	if( !req.session.me || req.session.me == null ){
		res.send( JSON.stringify({error:true, reason:"login"}) );
	}else{
		Wallet.myValue( {
			user_id: req.session.me
		}, function(_data){
			res.send( JSON.stringify(_data) );
		});
	}
});

var Util = {};
var conn = {};
router.init = function(_params){
	Util = _params.util;
	conn = Util.mysql;
}

module.exports = router;

var Wallet = {
	list: function(_params, _callback){
		var sql = "SELECT * "
			+ " ,IFNULL(init_amount, 0) AS init_amount "
			+ " ,IFNULL(coins, 0) AS coins "
			+ " FROM wallet WHERE user_id="+conn.escape(_params.user_id);
		Util.execQuery({
		sql: sql
		,error_log: "error selecting wallet"
		,callback: function(_data){
			return _callback(_data.res);
		}})
	}
	,add: function(_params, _callback){
		var sql = "INSERT INTO wallet SET ?"
		Util.execQuery({
		sql: sql
		,data: _params
		,error_log: "error inserting wallet"
		,callback: function(_res){
			return _callback({
				id: _res.insertId
			});
		}});
	}
	,update: function(_params, _callback){

		var sql = "UPDATE wallet SET coins="+conn.escape(_params.coins)
				+ " ,init_amount="+conn.escape(_params.init_amount)
				+ " WHERE id="+conn.escape(_params.id)
				+ " AND user_id="+conn.escape(_params.user_id)

		Util.execQuery({
		sql: sql
		,error_log: "error updating wallet"
		,callback: function(_res){
			return _callback(_res);
		}})
	}
	,remove: function(_params, _callback){
		var sql = "DELETE FROM wallet WHERE id="+conn.escape(_params.id)
			+ " AND user_id="+conn.escape(_params.user_id)
		Util.execQuery({
		sql: sql
		,error_log: "error deleting wallet"
		,callback: function(_res){
			return _callback({error:false});
		}})
	}
	,myValue: function(_params, _callback){
		Wallet.list(_params,function(_coins){
			var my_coin = {}
			for(var i in _coins){
				if( my_coin[_coins[i].currency_code] == undefined ){
					my_coin[_coins[i].currency_code] = {
						coins: 0
						,symbol: _coins[i].currency_code
						,currency: _coins[i].currency_description
						,init_amount: _coins[i].init_amount
						,wallet_id: _coins[i].id
					}
					my_coin[_coins[i].currency_code].coins += _coins[i].coins
				}
			}
			Wallet.myCoinValue({
				coins: my_coin
			},function(_coins){
				return _callback(_coins);	
			});
		});
	}
	,myCoinValue: function(_params, _callback){
		var codes = [];
		for(var i in _params.coins){
			codes.push(_params.coins[i].symbol);
		}
		var url = "https://min-api.cryptocompare.com/data/pricemulti?fsyms="
			+ codes.join(",")
			+ "&tsyms=AUD"

		console.log( url );

		request(url, function ( error, response, body) {
			if (!error && response.statusCode == 200){
				var values = JSON.parse(body);
				for(var c in values){
					_params.coins[c].current_value = values[c]['AUD']
					_params.coins[c].my_value = _params.coins[c].current_value * _params.coins[c].coins
				}
				return _callback( _params.coins )
			}else{
				console.log("ERROR")
				return _callback({error:true});
			}
			
		});

	}
};