var express = require('express');  
var router = express.Router();

var async = require('async');
var moment = require('moment-timezone');

router.initiated = false;

var conn = {};

router.init = function(_params){

	if( _params.mysql == undefined ){
		console.log("DB has not been defined");
		console.log("caller is " + arguments.callee.caller.toString());
	}

	Util.mysql = _params.mysql;				//	MySQL
	conn = _params.mysql;					// 	Use the one conn variable

	Util.db_name = process.env.CRYPTOCHECK_DB_NAME || "cryptocheck";

	router.mysql = Util.mysql;
	router.db_name = Util.db_name;
	router.initiated = true;
}

router.MySQLconnect = function(){
	return Util.mysql;
}
router.execQuery = function(_params){
	return Util.execQuery(_params);
}

module.exports = router;

var Util = {
	execQuery: function(_params){
		var sql = _params.sql != undefined ? _params.sql : "";
		var data = _params.data != undefined ? _params.data : {};
		var error_log = _params.error_log != undefined ? _params.error_log : "";
		var start_log = _params.start_log != undefined ? _params.start_log : "";
		var callback = _params.callback != undefined ? _params.callback : function(_response){};

		if( start_log != "" ){
			console.log( start_log );
		}
		conn.query(sql,data,function(err,res){
			if(err){
				console.log(error_log);
				console.log(err);
				return callback({error:true,reason:err});
			}else{
				return callback({error:false,res:res});
			}
		});
	}
}