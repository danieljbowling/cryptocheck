var express = require('express');  
var router = express.Router();

var request = require('request');

router.get('/', function(req, res, next) {
	res.send('User module');
});
router.get('/check', function(req, res, next) {
	if( req.session.me == undefined || req.session.me == null ){
		res.send( JSON.stringify({
			status: "expired"
		}) );
	}else{
		res.send( JSON.stringify({
			status: "current"
			,id: req.session.me
			,email: req.session.email
		}) );
	}
});
router.get('/logout', function(req, res, next) {
	req.session.me = null;
	res.send( JSON.stringify({status:"expired"}) );
});
router.post('/login', function(req, res, next) {
	req.session.me = null;
	User.login( req.body, function(_data){
		if( !_data.error ){
			req.session.me = _data.id;
			req.session.email = _data.email;
		}
		res.send( JSON.stringify(_data) );
	});
});
router.post('/add', function(req, res, next) {
	User.add( req.body, function(_data){
		res.send( JSON.stringify(_data) );
	});
});
router.get('/remove/:id', function(req, res, next) {
	User.remove( req.params, function(_data){
		res.send( JSON.stringify(_data) );
	});
});

var Util = {};
var conn = {};
router.init = function(_params){
	Util = _params.util;
	conn = Util.mysql;
}

module.exports = router;

var User = {
	check: function(_params, _callback){
		return _callback("Running OK");
	}
	,login: function(_params, _callback){
		var sql = "SELECT * FROM user WHERE email="+conn.escape(_params.email);
		Util.execQuery({
		sql: sql
		,error_log: "error selecting user with email "+_params.email
		,callback: function(_data){
			if( _data.res.length == 0 ){
				return _callback({error:true, reason:"no such user"})
			}
			var db_password = _data.res[0].password;
			var test_password = require('crypto').createHash('md5').update(_data.res[0].email+':'+_params.password).digest("hex");
			if( db_password != test_password ){
				return _callback({error:true, reason: "incorrect password"});
			}
			return _callback({
				error:false
				,id: _data.res[0].id
				,email: _data.res[0].email
			});
		}});
	}
	,list: function(_params, _callback){
		var sql = "SELECT * FROM user"
		Util.execQuery({
		sql: sql
		,error_log: "error selecting user"
		,callback: function(_data){
			return _callback(_data.res);
		}})
	}
	,add: function(_params, _callback){
		_params.password = require('crypto').createHash('md5').update(_params.email+':'+_params.password).digest("hex");
		var sql = "INSERT INTO user SET ? "
		Util.execQuery({
		sql: sql
		,data: _params
		,error_log: "error inserting user"
		,callback: function(_res){
			return _callback(_res);
		}});
	}
	,remove: function(_params, _callback){
		var sql = "DELETE FROM user WHERE id="+conn.escape(_params.id)
		Util.execQuery({
		sql: sql
		,error_log: "error deleting user"
		,callback: function(_res){
			return _callback(_res);
		}});
	}
};