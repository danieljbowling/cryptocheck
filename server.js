// modules =================================================
var https          	= require('https');
var http 			= require('http');

var fs        	  	= require('fs');

var express        	= require('express');
var app            	= express();
var bodyParser     	= require('body-parser');
var methodOverride 	= require('method-override');
var cookieSession 	= require('cookie-session');
var cookieParser 	= require('cookie-parser');
var busboy 			= require('connect-busboy');

var moment 			= require('moment-timezone');
var timeout 		= require('connect-timeout'); //express v4

var domain 			= require('domain');

// configuration ===========================================

var https_port = 8443;
var http_port = 8080;
var run_https = false;
var debug_mode = true;

var db = require('./config/db');		// Database Config

// Test increase file upload limit ///
app.use(bodyParser.json({ limit: '50mb' })); // parse application/json 
app.use(bodyParser.json({ limit: '50mb', type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true })); // parse application/x-www-form-urlencoded

app.use(methodOverride('X-HTTP-Method-Override')); // override with the X-HTTP-Method-Override header in the request. simulate DELETE/PUT
app.use(express.static(__dirname + '/public')); // set the static files location /public/img will be /img for users
app.use(busboy());		// For file uploads
app.use(cookieParser());

var sessionMiddleware = cookieSession({
	name: 'session',
	keys: ['key1', 'key2']
});

var errorFilter = function(err, req, res, next){
	if(!res.headersSent){ //just because of your current problem, no need to exacerbate it.
		errcode = err.status || 500; //err has status not statusCode
		msg = err.message || 'server error!';
		res.status(errcode).send(JSON.stringify({error:true,reason:"server error!"})); //the future of send(status,msg) is hotly debated
	}
};
app.use(errorFilter);
var timeout_seconds = 10 * 1000;

app.use(timeout(timeout_seconds));		// 10 seconds timeout

function haltOnTimedout(req, res, next){
	if (!req.timedout){
		next();
	}
}
app.use(haltOnTimedout);

//////////////////////////////////////////////

var d = domain.create();
d.on('error', function(err) {
	console.error(err);
});


app.http_port = http_port;
app.https_port = https_port;

var server = {};

var createServer = function(){
	if( run_https ){
		server = https.createServer({
			key: fs.readFileSync( key ),
			cert: fs.readFileSync( cert )
		}, app);
		server.listen(https_port);
	}else{
		server = http.createServer(app);
		server.listen(http_port);
	}
}

if( debug_mode ){
	console.log(" -------- DEBUG MODE ON -------- ");
	createServer();
}

d.run(function() {
	if( !debug_mode ){
		createServer();
	}
	app.use(sessionMiddleware);
	var params = {
		run_https: false
		,mysql: db.mysql
	};
	var routes = require('./app/routes')(app, params); // pass our application into our routes	
});

/////////////////////////////////////////////

// start app ===============================================
if( run_https ){
	console.log("Server running HTTPS on : "+https_port);
}else{
	console.log('Server running HTTP on : '+http_port);	
}
exports = module.exports = app; 						// expose app