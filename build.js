var mysql = require('mysql');
var Util = require('./app/modules/Util.js');
var async = require('async');
var moment = require('moment-timezone');

var conn = {};
var Build = {
	init: function(_callback){
		Util.init({mysql:Build.MySQLconnect()});
		conn = Util.mysql;
		if(_callback!=undefined){
			return _callback();
		}
	}
	,MySQLconnect: function(){
		return mysql.createPool({
			connectionLimit : 10,
			host            : process.env.CRYPTOCHECK_HOST || "localhost",
			user            : process.env.CRYPTOCHECK_DB_USER || "root",
			password        : process.env.CRYPTOCHECK_DB_PASS || "root",
			database        : process.env.CRYPTOCHECK_DB_NAME || "cryptocheck"
		});
	}
	,destroy: function(_callback){
		Build.allExistingTables({},function(_tables){
			async.eachSeries(_tables,function(table,next){
				Util.execQuery({
				sql: "DROP TABLE "+table
				,error_log: "error dropping table"
				,callback: function(_data){
					next();
				}});
			},function(err){
				return _callback();
			});
		});

	}
	,alterCol: function(_params,_callback){
		var table = _params.table;
		var col = _params.col;
		var type = _params.type;
		var is_null = _params.is_null != undefined ? _params.is_null : true;
		
		var sql = "ALTER TABLE "+table;
			sql+= " MODIFY COLUMN "+col;
			sql+= " "+type;
		if( is_null ){
			sql+= " NULL";
		}else{
			sql+= " NOT NULL";
		}

		conn.query(sql,function(err,result){
			return _callback(result);
		});
	}
	,addCol: function(_params,_callback){
		var self = this;
		var conn = self.MySQLconnect();

		var table = _params.table;
		var col = _params.col;
		var type = _params.type;

		var sql = "ALTER TABLE "+table;
			sql+= " ADD COLUMN "+col;
			sql+= " "+type;

		conn.query(sql,function(err,result){
			return _callback(result);
		});
	}
	,dropCol: function(_params,_callback){
		var self = this;
		var conn = self.MySQLconnect();

		var table = _params.table;
		var col = _params.col;
		var type = _params.type;

		var sql = "ALTER TABLE "+table;
			sql+= " DROP COLUMN "+col;

		conn.query(sql,function(err,result){
			return _callback(result);
		});
	}
	,createTable: function(_params, _callback){
		var table = _params.table;
		var sql = "CREATE TABLE IF NOT EXISTS " + table + " (";
			sql+= "id INTEGER NOT NULL AUTO_INCREMENT,";
			sql+= "PRIMARY KEY (id) );";
		Util.execQuery({
		sql: sql
		,error_log: "error creating table"
		,callback: function(_response){
			return _callback(_response);
		}});
	}
	,allExistingTables: function(_params,_callback){
		var sql = "SELECT * FROM information_schema.TABLES ";
			sql+= " WHERE TABLE_SCHEMA="+conn.escape(Util.db_name);
		Util.execQuery({
		sql: sql
		,error_log: "error listing all tables"
		,callback: function(_data){
			var tables = [];
			for(var i in _data.res){
				tables.push(_data.res[i].TABLE_NAME);
			}
			return _callback(tables);
		}});
	}
	,tableExists: function(_params, _callback){
		var table = _params.table;
		var sql = "SELECT * FROM information_schema.TABLES "
			+ " WHERE TABLE_SCHEMA="+conn.escape(Util.db_name)
			+ " AND TABLE_NAME="+conn.escape(table)
			+ " LIMIT 1;"
		Util.execQuery({
		sql: sql
		,error_log: "error checking if table exists"
		,callback: function(_response){
			
			if( _response.res.length==0){
				return _callback({exist:false});
			}else{
				sql = "SELECT DISTINCT ";
				sql+= " TABLE_NAME, INDEX_NAME ";
				sql+= " FROM INFORMATION_SCHEMA.STATISTICS ";
				sql+= " WHERE TABLE_NAME="+conn.escape(table);

				if(table=="users"){
					console.log("SQL for users");
					console.log(sql)
				}

				Util.execQuery({
				sql: sql
				,error_log: "error checking indexes"
				,callback: function(_res1){
					var indexes = [];
					for(var i in _res1.res ){
						indexes.push( _res1.res[i].INDEX_NAME );
					}
					if(table=="users"){
						console.log("Indexes for users")
						console.log(indexes);
					}
					return _callback({exist:true,indexes:indexes});
				}});
			}
		}});
	}
	,fieldExists: function(_params, _callback){
		var table = _params.table;
		var field = _params.field;
		var type = _params.type;
		var conn = Util.mysql;
		var sql = "SELECT * FROM information_schema.COLUMNS "
			+ " WHERE TABLE_SCHEMA="+conn.escape(Util.db_name)
			+ " AND TABLE_NAME="+conn.escape(table)
			+ " AND COLUMN_NAME="+conn.escape(field);

		var correct_type = false;
		var field_exist = false;
		var is_nullable = false;

		Util.execQuery({
		sql: sql
		,error_log: "error checking if the field exists"
		,callback: function(_response){
			// if( field == "from_id" ){
			// 	console.log(_response);
			// }
			if( _response.res.length == 1 ){
				field_exist = true;
				if( _response.res[0].DATA_TYPE.toLowerCase() == type.toLowerCase() ){
					correct_type = true;
				}
				if( _response.res[0].IS_NULLABLE == "YES" ){
					is_nullable = true;
				}
			}
			return _callback({exist:field_exist,type:correct_type,is_nullable:is_nullable});
		}});
	}
	,checkTable: function(_params, _callback){

		// ALTER TABLE listings DROP INDEX keywords;

		var table = _params.name;
		var fields = _params.fields;
		var existing_indexes = _params.existing_indexes != undefined ? _params.existing_indexes : [];

		if( table == "users" ){
			console.log("Indexes in users");
			console.log(existing_indexes);
		}
		if( table == "chats" ){
			console.log("chats existing indexes")
			console.log(existing_indexes);
		}
		async.eachSeries( fields, function(field, next){

			var index_sql = "";		// Just a nothing query to run

			if( field.index != undefined && field.index ){
				// console.log( field.field + " wants to be an INDEX" )
				// console.log( existing_indexes.indexOf(field.field) )

				if( existing_indexes.indexOf(field.field) == -1 ){	// Need to create this index
					index_sql = "CREATE INDEX "+field.field+" ON "+table+" ("+field.field+")";
					// console.log( index_sql );
				}
			}
			if( field.index == undefined || field.index == false ){
				if( existing_indexes.indexOf(field.field) != -1 ){	// Need to drop this index
					index_sql = "ALTER TABLE "+table+" DROP INDEX "+field.field+";"
					// console.log( index_sql );
				}
			}

			Build.fieldExists({
				table: table
				,field: field.field
				,type: field.type
			},function(_res){

				var is_nullable = field.is_nullable != undefined ? field.is_nullable : true;

				if( !_res.exist ){	// Add the col
					
					Build.addCol({
						table: table
						,col: field.field
						,type: field.type
						,is_nullable: is_nullable
					},function(_res){
						if( index_sql != "" ){
							console.log( index_sql );
							Util.execQuery({
							sql: index_sql
							,callback: function(_res1){
								next();
							}});
						}else{
							next();	
						}
					})
				}else if( !_res.type ){	// Alter the col

					Build.alterCol({
						table: table
						,col: field.field
						,type: field.type
						,is_nullable: is_nullable
					},function(_res){
						if( index_sql != "" ){
							console.log( index_sql );
							Util.execQuery({
							sql: index_sql
							,callback: function(_res1){
								next();
							}});
						}else{
							next();	
						}
					})
				}else if( is_nullable != _res.is_nullable ){
					Build.alterCol({
						table: table
						,col: field.field
						,type: field.type
						,is_nullable: is_nullable
					},function(_res){
						if( index_sql != "" ){
							console.log( index_sql );
							Util.execQuery({
							sql: index_sql
							,callback: function(_res1){
								next();
							}});
						}else{
							next();	
						}
					})
				}else{				// No change
					if( index_sql != "" ){
						console.log( index_sql );
						Util.execQuery({
						sql: index_sql
						,callback: function(_res1){
							next();
						}});
					}else{
						next();	
					}
				}

			});

		},function(err){
			return _callback({error:false});
		});
	}
	,readTemplate: function(_callback){
		var fs = require('fs');
		var obj;
		fs.readFile('db.json', 'utf8', function (err, data) {
			if (err) throw err;
			
			obj = JSON.parse(data)
			
			async.eachSeries( obj, function(table, next){
				
				Build.tableExists( {table:table.name}, function(_exist){
					if( !_exist.exist ){
						Build.createTable( {table:table.name}, function(_res){
							Build.checkTable( table, function(_data){
								next();
							});
						});
					}else{
						table.existing_indexes = _exist.indexes;
						Build.checkTable( table, function(_data){
							next();
						});
					}
				});
				
			},function(err){
				return _callback( {error:false} );
			});

		});
	}
};

var task = "";
if( process.argv[2] != undefined ){
	task = process.argv[2];
}

Build.init(function(){
	if( task == "init" ){
		Build.initUser(function(_data){
			Build.buildClassLookup(function(){
				process.exit(0);
			});
		});
	}
	if( task == "destroy" ){
		Build.destroy(function(){
			process.exit(0);
		});
	}
	if( task == "" ){
		console.log("Standard runner");
		Build.readTemplate(function(_data){
			console.log(_data);
			process.exit(0);
		});
	}
});