var mysql = require('mysql');

var mySqlPool = mysql.createPool({
	connectionLimit : 10,
	host            : process.env.CRYPTOCHECK_HOST || "localhost",
	user            : process.env.CRYPTOCHECK_DB_USER || "root",
	password        : process.env.CRYPTOCHECK_DB_PASS || "root",
	database        : process.env.CRYPTOCHECK_DB_NAME || "cryptocheck"
});

module.exports = {
	mysql: mySqlPool
}