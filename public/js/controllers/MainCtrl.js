(function () {
	'use strict';

	var MainController = function ($timeout, $scope, $rootScope, $http, $location, User, Wallet) {
		var vm = this;

		vm.update_wallet = false;

		vm.listWallets = function(){
			Wallet.list()
			.then(function(_data){
				console.log("wallet")
				console.log(_data);
				vm.wallets = _data;
			});
		}
		vm.myValue = function(){
			vm.loading_value = true;
			Wallet.myValue()
			.then(function(_data){
				vm.loading_value = false;
				vm.my_value = _data;
				vm.total_init_amount = 0;
				vm.total_value = 0;
				for( var i in _data ){
					vm.total_init_amount += _data[i].init_amount;
					vm.total_value += _data[i].my_value;
				}
				vm.change_value = vm.total_value - vm.total_init_amount;
			})
		}

		vm.selectUpdateWallet = function(_wallet){
			vm.update_wallet = true;
			vm.selected_wallet = _wallet;
		}

		vm.updateWallet = function(){
			Wallet.update({
				coins: vm.selected_wallet.coins
				,init_amount: vm.selected_wallet.init_amount
				,id: vm.selected_wallet.wallet_id
			}).then(function(_data){
				vm.update_wallet = false;
				vm.myValue();
			})
		}

		vm.init = function(){
			vm.listWallets();
			vm.myValue();
		}

		vm.init();
	}
	
	angular
		.module('sampleApp')
		.controller('MainController', MainController);

})();