(function () {
	'use strict';

var LoginController = function($rootScope, $location, User){
	var vm = this;
	vm.user_exists_error = false;
	vm.check_email = false;
	
	vm.init = function(){
		vm.username = $rootScope.lastLoginName;
	}
	vm.init();

	vm.login = function(){
		vm.dataLoading = true;
		User.login(vm.username, vm.password)
		.then(function(_data){
			console.log(_data)
			vm.dataLoading = false;
			if( _data.status == 'expired' ){
				vm.error = "Failed to login";
			}else{
				if( $rootScope.postLogInRoute ){
					window.location.href = $rootScope.postLogInRoute;
					$rootScope.postLogInRoute = null;
				}else{
					window.location.href = "/";
				}
			}
		});
	}

	vm.reVerifiy = function(){
		User.requestVerifyEmail(vm.email)
		.then(function(_data){
			console.log("requestVerifyEmail")
			console.log(_data);
			// vm.check_email = true;
		});
	}
};
angular
	.module('sampleApp')
	.controller('LoginController',LoginController);

})();