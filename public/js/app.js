(function(){
	'use strict';

	var inj = [];
	inj.push('ngRoute');
	inj.push('ngFileUpload');

	inj.push('UserService');
	inj.push('WalletService');
	inj.push('ui.bootstrap');
	inj.push('infinite-scroll');

	angular
		.module('sampleApp',inj)
		.config(config)
		.run(run)

	config.$inject = ['$routeProvider', '$locationProvider', '$httpProvider'];

	function config($routeProvider, $locationProvider, $httpProvider){	

		$httpProvider.defaults.cache = false;
		if (!$httpProvider.defaults.headers.get) {
			$httpProvider.defaults.headers.get = {};
		}
		// disable IE ajax request caching
		$httpProvider.defaults.headers.get['If-Modified-Since'] = '0';

		var postLogInRoute = null;

		var checkFirst = function(User,$rootScope,$location,$uibModal){	// ALL $rootScope set in UserService.js check !!!!

			$rootScope.loggedIn = false;

			$rootScope.liveAlertOpen = false;
			$rootScope.saveAlertOpen = false;
			$rootScope.save_alert_loading = false;
			$rootScope.save_alert_message = "Saved";

			return User.check().then(function(_data){
				// console.log( _data );
			});
		}

		$routeProvider
			.when('/',{
				templateUrl: 'views/home.html',
				controller: 'MainController',
				controllerAs: 'vm',
				resolve: { check: checkFirst }
			})
			.when('/login', {
				templateUrl: 'views/login.html',
				controller: 'LoginController',
				controllerAs: 'vm',
				resolve: { check: checkFirst }
			})
			.when('/not-found', {
				templateUrl: 'views/not-found.html',
			})		
			.otherwise({ redirectTo: '/not-found' });

		$locationProvider.html5Mode(true);
	}
	
	run.$inject = ['$rootScope', '$location', '$http', 'User', '$timeout', '$window', '$uibModal'];
	function run($rootScope, $location, $http, User, $timeout, $window, $uibModal) {


		$rootScope.spinnerImage = "data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==";
		
		$rootScope.$on('$stateChangeSuccess', function (event) {
		    $window.ga('send', 'pageview', $location.path());
		});

		$rootScope.logout = function(){
			
			$rootScope.loggedIn = false;

			User.logout($rootScope.userId)
			.then(function(_data){
				$rootScope.loggedIn = false;
				$timeout(function() {
					$rootScope.loggedIn = false;
				}, 100);
				window.location.href = "/login";
			});
		}
		
	}
	
angular
	.module('sampleApp')
	.directive('focusOnShow', function($timeout) {
		return {
			restrict: 'A',
			link: function($scope, $element, $attr) {
				if ($attr.ngShow){
					$scope.$watch($attr.ngShow, function(newValue){
						if(newValue){
							$timeout(function(){
								$element[0].focus();
							}, 0);
						}
					})      
				}
				if ($attr.ngHide){
					$scope.$watch($attr.ngHide, function(newValue){
						if(!newValue){
							$timeout(function(){
								$element[0].focus();
							}, 0);
						}
					})
				}

			}
		};
	});

angular
	.module('sampleApp')
	.service('ConfirmService', function($uibModal) {
		var service = {};
		service.open = function (text, onOk) {
			var modalInstance = $uibModal.open({
				templateUrl: 'myModalContent.html',
				controller: 'ModalConfirmCtrl',
				resolve: {
					text: function () {
						return text;
					}
				}
			});

			modalInstance.result.then(function (selectedItem) {
				onOk();
				}, function () {
			});
		};

		return service;
});


angular
	.module('sampleApp')
	.controller('ModalConfirmCtrl', function ModalConfirmCtrl($scope, $uibModalInstance, text){
		$scope.text = text;

		$scope.ok = function () {
			$uibModalInstance.close(true);
		};

		$scope.cancel = function () {
			$uibModalInstance.dismiss('cancel');
		};
	});

angular
	.module('sampleApp')
	.directive( "mwConfirmClick", function(ConfirmService){
		return {
			restrict: 'A',
			scope: {
				eventHandler: '&mwConfirmClick'
			},
			link: function(scope, element, attrs){
				element.unbind("click");
				element.bind("click", function(e) {
					var message = attrs.mwConfirmClickMessage ? attrs.mwConfirmClickMessage : "Are you sure?";
					ConfirmService.open(message, scope.eventHandler);
				});
			}
		}
	});

angular
	.module('sampleApp')
	.directive( "modalShow", function($parse,$rootScope){
		return {
			restrict: "A",
			link: function (scope, element, attrs) {
				//Hide or show the modal
				scope.showModal = function (visible, elem) {
					if (!elem)
						elem = element;
					if (visible && $rootScope.loggedIn){
						$(elem).modal("show");
						$rootScope.sideMenuOpen = true;
					}else{
						$(elem).modal("hide");
						$rootScope.sideMenuOpen = false;
					}	
				}
				//Watch for changes to the modal-visible attribute
				scope.$watch(attrs.modalShow, function (newValue, oldValue) {
					scope.showModal(newValue, attrs.$$element);
				});

				//Update the visible value when the dialog is closed through UI actions (Ok, cancel, etc.)
				$(element).bind("hide.bs.modal", function () {
					$parse(attrs.modalShow).assign(scope, false);
					if (!scope.$$phase && !scope.$root.$$phase)
						scope.$apply();
				});
			}
    		}
	});

})();
