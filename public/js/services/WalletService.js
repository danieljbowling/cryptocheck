angular.module('WalletService', []).factory('Wallet', function($http,$rootScope,$location) {

	return {

		list: function(){
			var url = "/api/wallet/list";
			return $http.get(url).then(function(res){return res.data;});
		}
		,myValue: function(){
			var url = "/api/wallet/value";
			return $http.get(url).then(function(res){return res.data;});
		}
		,update: function(_data){
			return $http({
				url: '/api/wallet/update',
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param(_data)
			})
			.then(function(res) {
				return res.data;
			});
		}
	}

});			