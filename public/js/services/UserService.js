angular.module('UserService', []).factory('User', function($http,$rootScope,$location) {
	var currentUserId;

	return {

		check: function(){
			$rootScope.loggedIn = false;
			$rootScope.liveAlertOpen = false;
			$rootScope.saveAlertOpen = false;
			$rootScope.save_alert_loading = false;
			$rootScope.save_alert_message = "Saved";

			var url = "/api/user/check";
			return $http.get(url)
			.then(function(_res){
				var _data = _res.data;
				console.log("User check")
				console.log(_data);
				if( _data.status == 'expired' ){	// Log them out
					$rootScope.loggedIn = false;
					if( $location.path() != '/login' ){
						$rootScope.postLogInRoute = $location.path();	// The one we wanted, but not allowed
						window.location.href = "/login";
					}
					$rootScope.lastLoginName = _data.usr;
				}else{			// Log in OK
					$rootScope.profile = _data;
					$rootScope.loggedIn = true;
					$rootScope.userId = _data.id;
					if( $location.path() == "/login" ){
						$location.path("/");
					}
				}			// END Log in OK
				return _res.data;
			});
		}
		,login: function(_email,_password){
			return $http({
				url: '/api/user/login',
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param({email:_email, password:_password})
			})
			.then(function(res) {
				return res.data;
			}, 
			function(res) { // optional
				return res.data;
			});
		}
		,logout: function(_user_id){
			var url = "/api/user/logout";
			return $http.get(url).then(function(res){return res.data;});
		}
	}

});